from flask import Flask, jsonify, render_template, request, url_for, redirect, session
from modelfunctions import *
from user import *
from post import *
from comment import *


app = Flask(__name__)

app.secret_key = 'fjdksalfhup3502rt/5342afdsa'

@app.route('/')
def index():
    return render_template('index.html', user=session.get('username'), posts=get_posts()[:5])


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        username = request.form['username_text']
        password = request.form['password_text']
        if check_login(username, password):
            session['username'] = username
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error='Incorrect username or password')
    else:
        return render_template('login.html')


@app.route('/signup', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        username = request.form['username_text']
        password_one = request.form['password_one']
        password_two = request.form['password_two']
        if check_username_aval(username) and compare_passwords(password_one, password_two):
            create_user(username, password_one)
            return redirect(url_for('index'))
        elif not check_username_aval(username):
            return render_template('signup.html', error='Username not available')
        else:
            return render_template('signup.html', error='Passwords don\'t match')
    else:
        return render_template('signup.html')


@app.route('/logout', methods=['GET'])
def logout():
    session.pop('username')
    return redirect(url_for('index'))


# will be route for editing, posting, deleting, and commenting a post
@app.route('/post', methods=['GET', 'POST'])
def post():
    if request.method == 'POST':
        create_post(
            request.form['title'],
            request.form['content'],
            session.get('username')
        )
        return(redirect(url_for('index')))
    return(redirect(url_for('index')))


@app.route('/post/new')
def new_post():
    return render_template('post.html', edit_mode=False, user=session.get('username'), post='')


@app.route('/post/<int:post_id>', methods=['GET', 'POST'])
def show_post(post_id):
    if request.method == 'GET':
        post = get_post(post_id)
        if post[3] == session.get('username'):
            return render_template('one_post.html', post=get_post(post_id),
                                   user=session.get('username'), edit_mode=True, comments=get_comments(post_id)[:10])
        else:
            return render_template('one_post.html', post=get_post(post_id),
                                   user=session.get('username'), edit_mode=False, comments=get_comments(post_id)[:10])
    else:
        update_post(
            request.form['title'],
            request.form['content'],
            post_id
        )
        return (redirect(url_for('index')))


@app.route('/post/<int:post_id>/edit', methods=['GET'])
def edit_post(post_id):
    post = get_post(post_id)
    if post[3] == session.get('username'):
        return render_template('post.html', edit_mode=True, user=session.get('username'), post=post)
    else:
        return redirect(url_for('index'))


@app.route('/post/<int:post_id>/delete', methods=['GET'])
def delete_post(post_id):
    delete_post_by_id(post_id)
    return redirect(url_for('index'))


@app.route('/post/<int:post_id>/comment', methods=['POST'])
def new_comment(post_id):
    create_comment(
        request.form['content'],
        session.get('username'),
        post_id
    )
    return redirect(url_for('show_post', post_id=post_id))


@app.route('/post/<int:post_id>/<int:comment_id>/edit', methods=['GET', 'POST'])
def edit_comment(post_id, comment_id):
    if request.method == 'GET':
        return render_template('edit_comment.html', user=session.get('username'),
                               post=get_post(post_id) ,comment=get_comment(comment_id))
    else:
        update_comment(comment_id, request.form['content'])
        return redirect(url_for('show_post', post_id=post_id))


@app.route('/post/<int:post_id>/<int:comment_id>/delete')
def delete_comment(post_id, comment_id):
    delete_comment_by_id(comment_id)
    return redirect(url_for('show_post', post_id=post_id))


if __name__ == '__main__':
    app.run(debug=True)
