import sqlite3


def create_comment(data, user_id, post_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    c.execute('INSERT INTO comments (content, author, post) VALUES (?, ?, ?)',
              (data, user_id, post_id))
    conn.commit()
    pass


def get_comment(comment_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    return c.execute('SELECT * FROM comments WHERE id = (?)', (comment_id,)).fetchone()


def get_comments(post_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    return c.execute('SELECT * FROM comments WHERE post = (?) ORDER BY id DESC', (post_id,)).fetchall()


def update_comment(comment_id, data):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    c.execute('UPDATE comments SET content = (?) WHERE id = (?)', (data, comment_id,))
    conn.commit()
    pass


def delete_comment_by_id(comment_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    c.execute('DELETE FROM comments WHERE id = (?)', (comment_id,))
    conn.commit()
    pass

