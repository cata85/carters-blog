import sqlite3


conn = sqlite3.connect('userdata.db')

c = conn.cursor()


c.execute('''CREATE TABLE IF NOT EXISTS users(
             id INTEGER PRIMARY KEY,
             username TEXT UNIQUE NOT NULL,
             password TEXT NOT NULL
             )
         ''')

c.execute('''CREATE TABLE IF NOT EXISTS posts(
             id INTEGER PRIMARY KEY,
             title TEXT NOT NULL,
             content TEXT,
             author INTEGER REFERENCES users(id)
             )
         ''')

c.execute('''CREATE TABLE IF NOT EXISTS comments(
             id INTEGER PRIMARY KEY,
             content TEXT NOT NULL,
             author INTEGER REFERENCES users(id),
             post INTEGER REFERENCES posts(id)
             )
         ''')
