import sqlite3


def create_post(title, content, user):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    c.execute('INSERT INTO posts (title, content, author) VALUES (?, ?, ?)',
              (title, content, user))
    conn.commit()
    pass


def get_post(post_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    return c.execute('SELECT * FROM posts WHERE id = (?)', (post_id,)).fetchone()


def get_posts():
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    return c.execute('SELECT * FROM posts ORDER BY id DESC').fetchall()


def update_post(title, content, post_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    c.execute('UPDATE posts SET title = ?, content = ? WHERE id = ?',
                     (title, content, post_id))
    conn.commit()
    pass


def delete_post_by_id(post_id):
    conn = sqlite3.connect('userdata.db')
    c = conn.cursor()

    c.execute('DELETE FROM posts WHERE id = ?', (post_id,))
    conn.commit()
    pass
